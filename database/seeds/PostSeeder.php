<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();
        
        for ($i= 1; $i < 10; $i++) {
        Post::create([
            'title'   => 'Title '.$i,
            'body'    => 'Body '.$i,
            'user_id' => 2,
        ]);
    }
    }
}
