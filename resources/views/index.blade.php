@extends('layouts.app')
    <div class="content">
        @section('content')
            <div id="content">
                <ul>
                    @foreach ($posts as $post)
                    <li>
                        <div class="title">
                            <a href="{{ url('post/'.$post->id) }}">
                                <h4>{{ $post->title }}</h4>
                            </a>
                        </div>
                        <div class="body">
                            <p>{{ $post->body }}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        @endsection
    </div>
