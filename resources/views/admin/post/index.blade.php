@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header">
                    Manage Post
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="flash-message">
                              @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                              @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url('admin/posts/create') }}" class="btn btn-lg btn-primary float-right">Add New</a>    
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <td>Title</td>
                            <td>Content</td>
                            <td>Action</td>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr><th>{{ $post->title }}</th>
                                    <th> {{ $post->body }}</th>       
                                    <td><a href="{{ url('admin/posts/'.$post->id.'/edit') }}" class="btn btn-success">Update</a>
                                        <form action="{{ url('admin/posts/'.$post->id) }}" method="POST" style="display: inline;">
                                            {{ method_field('DELETE') }}
                                            @csrf
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        <tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
