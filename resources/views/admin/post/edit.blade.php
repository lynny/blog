@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card panel-default">
                <div class="card-header">Manage Post</div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="flash-message">
                              @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                              @endforeach
                            </div>
                        </div>
                    </div>
                    <form action="{{ url('admin/posts/'.$post->id)}}" method="POST">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class='form-group'>
                            <input type="text" name="title" class="form-control" required="required" placeholder="Title" value="{{$post->title}}">
                        </div>
                        <div class='form-group'>
                            <textarea name="body" rows="10" class="form-control" required="required" placeholder="Body">
                                {{$post->body}}
                            </textarea>
                        </div>
                        <button class="btn btn-lg btn-info">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection