@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card panel-default">
                <div class="card-header">Add a new post</div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="flash-message">
                              @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                              @endforeach
                            </div>
                        </div>
                    </div>
                    <form action="{{ url('admin/posts') }}" method="POST">
                        @csrf
                        <div class='form-group'>
                            <input type="text" name="title" class="form-control" required="required" placeholder="Title" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class='form-group'>
                            <textarea name="body" rows="10" class="form-control" required="required" placeholder="Body">
                                {{{ old('body') }}}
                            </textarea>
                        </div>
                        <button class="btn btn-lg btn-info">Post</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection