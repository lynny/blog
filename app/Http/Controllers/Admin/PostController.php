<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use \App\Post;
use Illuminate\Http\Request;
use Session;

/**
 * Description of PostController
 *
 * @author Lynn
 */
class PostController extends Controller{
    
    public function index()
    {
        return view('admin/post/index')->withPosts(Post::all());
    }
    
    public function create()
    {
        return view('admin/post/create');
    }
    
    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);
        
        $post = new Post;
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->user_id = $request->user()->id;

        if ($post->save()) {
            Session::flash('alert-success', 'Your post has been published.');
            return redirect('admin/posts');
        } else {         
            Session::flash('alert-danger', 'Failed in publish your post.');
            return redirect()->back()->withInput();
        }
    }
    
    public function edit($id){
        $post = Post::find($id);
        return view('admin/post/edit')->withPost($post);
    }
    
    public function update($id,Request $request){
        $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
        ]);      
        
        $post = Post::find($id);
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        if ($post->save()) {
            Session::flash('alert-success', 'Your post has been updated.');
            return redirect('admin/posts');
        } else {         
            Session::flash('alert-danger', 'Failed in publish your post.');
            return redirect()->back()->withInput();
        }
    }
    
    public function destroy($id){
        Post::find($id)->delete();
        Session::flash('alert-success', 'Your post has been deleted.');
        return redirect()->back()->withInput();
    }
}
